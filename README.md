
## Intro

I've spun up several Kubernetes clusters in the past using Virtual Box, ESXi, and now Proxmox. After trying to figure out a way to document this for a friend of mine, I ran into hiccups that I felt would be best served recorded.  Why? Because we all learn in the end.

## Choosing your Operating System

In this build, I used Ubuntu, but I'll record my steps later on Rocky and AlmaLinux (RHL variants). Pretty simple.

## Build

- Host OS: [Proxmox *.*](https://www.proxmox.com/en/downloads)
- VM OS: [Ubuntu Server](https://ubuntu.com/download/server#downloads)
- Controller Build: 
	- CPU: 2 core
	- Memory: 4 Gig
	- Storage: 32 Gig
	- Network: Bridge
		- kept it simple and set DHCP reservations for the controller. Yes, could use static IP, but reserving does the same thing and it hasn't broken my builds
	- Note: Minimum install for a controller
	
- Worker Build: 
	- CPU: 1 core
	- Memory: 2 Gig
	- Storage: 32 Gig
	- Network: Bridge
		- kept it simple and set DHCP reservations for the controller. Yes, could use static IP, but reserving does the same thing and it hasn't broken my builds
	- Note: Minimum install for a worker
	
I started with 1 worker node. once I got the main setup completed before joining the controller, I cloned the VM for continued build planning.

## Getting Servers ready

After spinning up the VM, my first goal was to do updates to the OS.
```
sudo apt update -y && sudo apt upgrade -y
sudo reboot
```
Make sure OpenSSH-Server is installed and enabled in your VM
```
sudo apt install openssh-server
sudo systemctl status ssh
sudo systemctl start ssh
```
Because I don't like guessing what IPs are on VMs via Proxmox, I like installing the Qemu-guest-agent onto my units. Makes it easier to find the info when you click your VM and go to the summary. 
```
sudo apt install qemu-guest-agent
sudo systemctl start qemu-guest-agent
```
Another item I like doing, with a little planning for Kubernetes is to add the IP/hostname into the host file. why? just an old habit for Kubernetes to know where to point for workers and controllers. probably a redundant step, but I find it works. 
```
sudo nano /etc/hosts
```
Add the worker and controller IP/hostname into the host file
```
192.168.99.101	k8s-controller
192.168.99.102	k8s-worker-1
```
next is to install a container runtime as we will need. without one, no containers in Kubernetes.
```
sudo apt install containerd

```
create the initial configuration
```
sudo mkdir /etc/containerd
containerd config default | sudo tee /etc/containerd/config.toml
```
For this cluster to work properly, we’ll need to enable SystemdCgroup within the configuration. To do that, we’ll need to edit the config we’ve just created:
```
sudo nano /etc/containerd/config.toml
```
Within that file, find the following line of text:
```
[plugins."io.containerd.grpc.v1.cri".containerd.runtimes.runc.options]
```
Underneath that, find the SystemdCgroup option and change it to true, which should look like this:
```
SystemdCgroup = true
```

next, we will need to disable swapping.
Kubernetes requires swap to be disabled. To turn off swap, we can run the following command:
```
sudo swapoff -a
```
Next, edit the 
```
sudo nano /etc/fstab
```
 file and comment out the line that corresponds to swap (if present). This will help ensure swap doesn’t end up getting turned back on after reboot.
```
sudo reboot
```
To enable bridging, we only need to edit one config file:
```
sudo nano /etc/sysctl.conf
```
Within that file, look for the following line:
```
#net.ipv4.ip_forward=1
```
Uncomment that line by removing the # symbol in front of it, which should make it look like this:
```
net.ipv4.ip_forward=1
```

The next step is to enable br_netfilter by editing yet another config file:
```
sudo nano /etc/modules-load.d/k8s.conf
```
Add the following to that file (the file should be empty at first):
```
br_netfilter
```

at this stage, it's a good idea to restart your servers
```
sudo reboot
```


## Install Kubernetes packages

The next step is to install the packages that are required for Kubernetes. First, we’ll add the required GPG key:
```
sudo curl -fsSLo /usr/share/keyrings/kubernetes-archive-keyring.gpg https://packages.cloud.google.com/apt/doc/apt-key.gpg
```
now at the time of this writing, Kubeadmin, kubectl, and kubelet are on version 1.25.0, I've had difficulty getting 1.25.0 to load up via apt-get, and HAVE resorted to using snap a few timers. But found a way around that. 
If things go well, this should work
```
sudo apt install kubeadm kubectl kubelet
```
if you're like me and get errors like the following:
```
stone-local@k8s-worker:~$ sudo apt install kubeadm kubectl kubelet
[sudo] password for gpadmin-local: 
Reading package lists... Done
Building dependency tree... Done
Reading state information... Done

No apt package "kubeadm", but there is a snap with that name.
Try "snap install kubeadm"


No apt package "kubectl", but there is a snap with that name.
Try "snap install kubectl"


No apt package "kubelet", but there is a snap with that name.
Try "snap install kubelet"

E: Unable to locate package kubeadm
E: Unable to locate package kubectl
E: Unable to locate package kubelet
```
for whatever reason, I could not get past this point. You could go the snap route, but I wanted to keep to the apt-get direction. So instead, my workaround was to do an apt-get install on an older version and upgrade
```
sudo apt update && sudo apt install -y kubeadm=1.24.6-00 kubelet=1.24.6-00 kubectl=1.24.6-00
```


##Clone your worker node

At this point, on the worker node, STOP. do the Checklist.
```
APT Updates and upgrades - Check
Add qemu-agent to all nodes - Check
Add nodes IP/hostname to the hostfile - Check
Pre-work for Kubernetes - Check  
Install kubeadm, kubelet, and kubectl
do another apt update and upgrade to upgrade kubeadm, kubelet, and kubectl
```

at this point,  sitting pretty on most of your nodes. now is a good time to clone your worker node so that you don't have to repeat the steps above too much. but to do that, we have to prep the worker node a little. 
```
sudo cloud-init clean
sudo rm -rf /var/lib/cloud/instances
sudo truncarte -s 0 /etc/machine-id
sudo rm /var/lib/dbus/machine-id
sudo ln -s /etc/machine-id /var/lib/dbus/machine-id
ls -l /var/lib/dbus/machine-id
sudo shutdown
```
now the worker node you created is up to snuff. the main goal here is that we want our clones to spin up but not be sharing the same ID or IP address as the original. we want this setup so that you can spin up as many worker nodes as you feel you need. 

Once your worker node is turned off. Browse back to your Proxmox web UI (https://<ip.address>:8006). look for the worker node/VM. It should be turned off.  if it's not turned off and just sits there endlessly, unlock, and stop the unit via SSH'ing to the proxmox host box. (example: ssh <username>@<ip.address>)
 
```
qm unlock <machine id>
qm stop <machine id>
```

From the Proxmox GUI, right-click the worker VM, and "Convert to the template". It will take a few minutes, depending on your hardware. Once it's complete, right-click the Work VM (it's still turned off) and select "Clone". 
You'll be brought to a window to clone from the template you created earlier. Give it a new VM ID, and name, and make sure the mode is selected to "Full Clone".  It should take a minute or two to spin the clone VM. now you have another worker node.  I usually leave the template unit by itself and never turn it on. 


## Kubernetes Controller Only

So long as you have everything complete so far, you can initialize the Kubernetes cluster now. Be sure to customize the first IP address shown here (not the second) and also change the name to match the name of your controller.
```
sudo kubeadm init --control-plane-endpoint=192.168.99.101 --node-name k8s-controller --pod-network-cidr=10.244.0.0/16
```
After the initialization finishes, you should see at least four commands printed within the output.

```
o start using your cluster, you need to run the following as a regular user:

  mkdir -p $HOME/.kube
  sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
  sudo chown $(id -u):$(id -g) $HOME/.kube/config

Alternatively, if you are the root user, you can run:

  export KUBECONFIG=/etc/kubernetes/admin.conf

You should now deploy a pod network to the cluster.
Run "kubectl apply -f [podnetwork].yaml" with one of the options listed at:
  https://kubernetes.io/docs/concepts/cluster-administration/addons/
```
Then you can join any number of worker nodes by running the following on each as root:
```
sudo kubeadm join 192.168.99.101:6443 --token ******.********** --discovery-token-ca-cert-hash *********************************** 
root@tf1-controller:/home/bob# 
```

the one we really care about is the sudo kubeabm. copy that. 

## Kubernetes Worker Only

Now that we have the key, lets add it to our worker node. 
```
sudo kubeadm join 192.168.99.101:6443 --token ******.********** --discovery-token-ca-cert-hash
```
if you've followed the steps above, this should join the worker node to the controller

##Run kubectl as a non-root user
If you want to be able to run kubectl commands as non-root user, then as a non-root user perform these
```
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config
```

##Conclusion
Validate that the controller can see all the worker nodes that wanted to join earlier.
```
kubectl get nodes
```
